#include "../include/image.h"
#include <malloc.h>
#include <stdbool.h>

bool createEmptyImage(struct image *image, uint64_t width, uint64_t height) {
    image->data = malloc(sizeof(struct pixel) * width * height);
    if (!image->data)
        return false;

    image->width = width;
    image->height = height;

    return true;
}
