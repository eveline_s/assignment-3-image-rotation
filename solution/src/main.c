#include "../include/bmp.h"
#include "../include/error.h"
#include "../include/rotate.h"
#include <assert.h>
#include <stdlib.h>


#define ARG_COUNT 4
#define INPUT_FILE_NAME 1
#define OUTPUT_FILE_NAME 2
#define ANGLE 3
#define MAX_ANGLE 270
#define MIN_ANGLE (-270)
#define ONE_ROTATE 90
#define NUMBER_FORMAT 10

int main(int argc, char **argv) {
    assert(argc == ARG_COUNT && "Error in arguments");

    FILE *in = fopen(argv[INPUT_FILE_NAME], "rb");
    assert(in != NULL && "Error in open file");

    struct image img = {0};

    enum read_status rStatus = from_bmp(in, &img);
    assert(rStatus == READ_OK && readStatus(rStatus));
    printf("%s\n", readStatus(rStatus));

    fclose(in);

    struct image result = {0};
    int angle = (int) strtol(argv[ANGLE], NULL, NUMBER_FORMAT);
    bool flag = false;
    for (int i = MIN_ANGLE; i <= MAX_ANGLE; i += ONE_ROTATE)
        if (angle == i) flag = true;

    assert(flag && "Error in angle");
    result = rotate(img, angle);
    free(img.data);

    FILE *out = fopen(argv[OUTPUT_FILE_NAME], "wb");
    assert(out != NULL && "Error in open file");

    enum write_status wStatus = to_bmp(out, &result);

    assert(wStatus == WRITE_OK && writeStatus(wStatus));
    printf("%s\n", writeStatus(wStatus));

    fclose(out);
    free(result.data);

    return 0;
}
