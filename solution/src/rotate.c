#include "../include/rotate.h"
#include "../include/image.h"
#include <malloc.h>

#define LOGIC_FUNC_ARGS struct image *tmp, const struct image *source

static void fillInEmptyImg(LOGIC_FUNC_ARGS, uint64_t height, uint64_t width) {
    tmp->data[height * source->width + width] = source->data[height * source->width + width];
}

static void fillInTmpImg(LOGIC_FUNC_ARGS, uint64_t height, uint64_t width) {
    source->data[height * source->width + width] = tmp->data[width * tmp->width + height];
}

static void LeftAngleRotate(LOGIC_FUNC_ARGS, uint64_t height, uint64_t width) {
    source->data[height * source->width + width] = tmp->data[(tmp->height - height - 1) * tmp->width + width];
}

static void rotatePixels(void (*func)(LOGIC_FUNC_ARGS, uint64_t height, uint64_t width), LOGIC_FUNC_ARGS) {
    for (uint64_t currentHeight = 0; currentHeight < source->height; currentHeight++)
        for (uint64_t currentWidth = 0; currentWidth < source->width; currentWidth++)
            func(tmp, source, currentHeight, currentWidth);
}

struct image rotate(const struct image source, int angle) {
    if (angle <= 0) angle += 360;
    int turnsCount = angle / 90;

    struct image rotatedImage = {0};
    createEmptyImage(&rotatedImage, source.width, source.height);
    rotatePixels(fillInEmptyImg, &rotatedImage, &source);

    for (int i = 0; i < turnsCount; i++) {
        struct image tmp = {0};
        createEmptyImage(&tmp, rotatedImage.height, rotatedImage.width);
        rotatePixels(fillInTmpImg, &rotatedImage, &tmp);

        free(rotatedImage.data);
        rotatedImage = tmp;

        createEmptyImage(&tmp, rotatedImage.width, rotatedImage.height);
        rotatePixels(LeftAngleRotate, &rotatedImage, &tmp);

        free(rotatedImage.data);
        rotatedImage = tmp;
    }

    return rotatedImage;
}

