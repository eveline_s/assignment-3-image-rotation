#include "../include/error.h"

const char* readStatus(enum read_status status) {
    switch (status){
        case READ_OK:
            return "Read file is ok";
        case READ_INVALID_SIGNATURE:
            return "Read invalid signature";
        case READ_INVALID_BITS:
             return "Read invalid bits";
        case READ_INVALID_HEADER:
              return "Read invalid header";
        case READ_MEMORY_ERROR:
            return "Read memory error";
        case READ_ERROR:
            return "Read error";
        default: {
            return "";
        }
    }
}
   

const char* writeStatus(enum write_status status) {
    switch (status){
        case WRITE_OK:
            return "Write is ok";
        case WRITE_ERROR:
            return "Write error";
        default: {
            return "";
        }
    }
}


    
