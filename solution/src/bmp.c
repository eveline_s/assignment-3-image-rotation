#include "../include/bmp.h"
#include "../include/error.h"

#define BF_TYPE 0x4D42
#define B_OFF_BITS 54
#define BI_BIT_COUNT 24
#define BI_SIZE 40
#define ZERO_PARAMS 0
#define bI_PLANES 1

static long getPadding(uint32_t width) {
    long padding = (long)(width * sizeof(struct pixel));
    padding = 4 - (padding % 4);
    padding = padding % 4;

    return padding;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header),1, in)) return READ_INVALID_HEADER;
    if (header.bfType != BF_TYPE) return READ_INVALID_SIGNATURE;
    if (header.bOffBits != B_OFF_BITS) return READ_INVALID_SIGNATURE;
    if (header.biBitCount != BI_BIT_COUNT) return READ_INVALID_HEADER;
    if (fseek(in, (long) header.bOffBits, SEEK_SET)) return READ_INVALID_HEADER;
    if (!createEmptyImage(img, header.biWidth, header.biHeight)) return READ_INVALID_BITS;

    long padding = getPadding(header.biWidth);
    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread((img->data) + i * header.biWidth, sizeof(struct pixel), header.biWidth, in) != header.biWidth) return READ_INVALID_BITS;
        if (fseek(in, padding, SEEK_CUR)) return READ_ERROR;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    long padding = getPadding(img->width);
    struct bmp_header header = getHeader(img->width, img->height, padding);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) return WRITE_ERROR;
        if (fseek(out, padding, SEEK_CUR)) return WRITE_ERROR;
    }

    return WRITE_OK;
}

struct bmp_header getHeader(uint64_t width, uint64_t height, long padding) {
    struct bmp_header header = {0};

    header.bfType = BF_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * (width + padding) * height;
    header.bfReserved = ZERO_PARAMS;
    header.bOffBits = B_OFF_BITS;
    header.biSize = BI_SIZE;
    header.biWidth = (uint32_t) width;
    header.biHeight = (uint32_t) height;
    header.biPlanes = bI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = ZERO_PARAMS;
    header.biSizeImage = sizeof(struct pixel) * (width + padding) * height;
    header.biXPelsPerMeter = ZERO_PARAMS;
    header.biYPelsPerMeter = ZERO_PARAMS;
    header.biClrUsed = ZERO_PARAMS;
    header.biClrImportant = ZERO_PARAMS;

    return header;
}
