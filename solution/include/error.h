#ifndef PL_LAB1_ERROR_H
#define PL_LAB1_ERROR_H

#include "bmp.h"
#include <stdio.h>

const char* readStatus(enum read_status status);

const char* writeStatus(enum write_status status);

#endif //PL_LAB1_ERROR_H
