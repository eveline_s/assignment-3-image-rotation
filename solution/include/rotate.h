#ifndef PL_LAB1_ROTATE_H
#define PL_LAB1_ROTATE_H

#include <stdint.h>

struct image rotate(struct image source, int angle);

#endif //PL_LAB1_ROTATE_H
