#ifndef PL_LAB1_IMAGE_H
#define PL_LAB1_IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

bool createEmptyImage(struct image *image, uint64_t width, uint64_t height);

#endif //PL_LAB1_IMAGE_H
